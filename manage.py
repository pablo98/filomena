#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "filomena.settings_" + os.environ.get('FILOMENA_ENV'))
    try:
        from django.core.management import execute_from_command_line
    except ImportError:
        raise

    execute_from_command_line(sys.argv)
