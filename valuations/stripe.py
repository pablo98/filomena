import logging

import stripe

from django.conf import settings
from django.utils import timezone

from valuations.models import StripeTransaction

stripe.api_key = settings.STRIPE_KEYS['secret_key']
logger = logging.getLogger('filomena')


def charge_service(amount, email, token, valuation):
    '''
    Asks Stripe to do actual charges.
    Policy is to log all eventual error details and re-raise
    exception with user-friendly messages
    '''
    try:
        customer = stripe.Customer.create(
            email=email,
            source=token
        )

        charge = stripe.Charge.create(
            customer=customer.id,
            amount=amount,
            currency='eur',
            description='Amsterprice'
        )

        transaction = _get_stripe_transaction_object(customer, charge, valuation)
        transaction.save()

        return transaction

    except stripe.error.CardError as e:
        # Since it's a decline, stripe.error.CardError will be caught
        body = e.json_body
        err = body['error']

        msg = \
            "Status is: {}".format(e.http_status) + \
            "Type is: {}".format(err['type']) + \
            "Code is: {}".format(err['code']) + \
            "Param is: {}".format(err['param']) + \
            "Message is: {}".format(err['message'])
        logger.error(msg)

        raise Exception('Card error.' + str(e))

    except stripe.error.RateLimitError as e:
        logger.error(str(e))
        raise Exception('Error while charging. Please try again.')

    except stripe.error.InvalidRequestError as e:
        logger.error(str(e))
        raise Exception('Error while charging. Please try again later.')

    except stripe.error.AuthenticationError as e:
        logger.error(str(e))
        raise Exception('Error while charging. Please try again later.')

    except stripe.error.APIConnectionError as e:
        logger.error(str(e))
        raise Exception('Communication error. Please try again later.')

    except stripe.error.StripeError as e:
        logger.error(str(e))
        raise Exception('Payment error. Please try again later.')

    except Exception as e:
        logger.error(str(e))
        raise Exception('Unknown error. Please try again later.')


def _get_stripe_transaction_object(customer, charge, valuation):
    transaction_dict = {
        'created_at': timezone.now(),
        'price': charge['amount'],
        'valuation': valuation,
        'approved': 1 if charge['paid'] is True else 0,
        'customer': customer,
        'charge': charge
    }
    transaction = StripeTransaction(**transaction_dict)

    return transaction
