from django.core.mail import EmailMessage
from django.template.loader import render_to_string

import valuations.form as valuation_form


def send_text_valuation_email(email, valuation):
    tmpl_vars = get_template_variables(valuation)
    subject = 'House price estimation'
    from_email = 'Amsterprice.com <info@amsterprice.com>'
    to = email
    text_content = render_to_string('emails/valuation.txt', tmpl_vars)
    msg = EmailMessage(subject, text_content, from_email, [to])
    msg.send()


def get_template_variables(v):
    return {
        'postcode': v.postcode,
        'floor': valuation_form.get_floor_description(v.floor_number, v.lift),
        'indoor_area': '{} squared meters'.format(v.indoor_area),
        'garden_area': '{} squared meters'.format(v.garden_area) if v.garden_area else 'No garden',
        'annual_lease': '{} EUR'.format(v.annual_lease_amount) if v.annual_lease_amount else 'No annual leasehold',
        'energielabel': '"' + v.energielabel + '"',
        'predicted_value': v.predicted_value
    }
