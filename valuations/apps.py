from django.apps import AppConfig


class ValuationsConfig(AppConfig):
    name = 'valuations'
