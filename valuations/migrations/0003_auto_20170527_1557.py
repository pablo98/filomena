# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-27 13:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('valuations', '0002_valuation_retrieved_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='valuation',
            name='retrieved_date',
            field=models.DateTimeField(null=True, verbose_name='date retrieved'),
        ),
    ]
