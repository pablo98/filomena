# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-08-08 19:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('valuations', '0006_auto_20170710_2341'),
    ]

    operations = [
        migrations.AddField(
            model_name='valuation',
            name='session_cookie',
            field=models.CharField(max_length=255, null=True),
        ),
    ]
