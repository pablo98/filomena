import logging

from django.utils import timezone

from valuations.models import Coupon, CouponTransaction

logger = logging.getLogger('filomena')


def redeem_coupon(coupon_code, valuation):
    coupon = Coupon.objects.get(code=coupon_code)
    if coupon.transactions_left <= 0:
        raise Exception('No transactions left for coupon ' + coupon_code)

    transaction = _generate_coupon_transaction_object(coupon, valuation)
    transaction.save()
    coupon.transactions_left -= 1
    coupon.save()

    return transaction


def _generate_coupon_transaction_object(coupon, valuation):
    transaction_dict = {
        'created_at': timezone.now(),
        'valuation': valuation,
        'coupon': coupon
    }
    transaction = CouponTransaction(**transaction_dict)

    return transaction
