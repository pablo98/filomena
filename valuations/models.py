from django.contrib.postgres.fields import JSONField
from django.core.validators import RegexValidator
from django.db import models


class Valuation(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    paid_at = models.DateTimeField(null=True)
    retrieved_at = models.DateTimeField(null=True)
    token = models.CharField(max_length=255)

    # input form fields
    postcode = models.CharField(
        validators=[RegexValidator(regex='\d{4}[A-Za-z]{2}')],
        verbose_name='Postcode',
        max_length=6)
    floor_number = models.IntegerField()
    indoor_area = models.IntegerField()
    garden_area = models.IntegerField()
    energielabel = models.CharField(max_length=1, verbose_name='Energy Label')
    lift = models.IntegerField(verbose_name='Has lift')
    annual_lease_amount = models.IntegerField(verbose_name='Annual lease amount')

    # output
    predicted_value = models.IntegerField()

    # user data
    user_fingerprint = models.CharField(max_length=20, null=True)


class Coupon(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    code = models.CharField(max_length=20)
    transactions_left = models.IntegerField()

    class Meta:
        indexes = [
            models.Index(fields=['code'])
        ]


class CouponTransaction(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    coupon = models.ForeignKey(Coupon)
    valuation = models.ForeignKey(Valuation, on_delete=models.CASCADE)


class StripeTransaction(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    price = models.IntegerField('price')
    valuation = models.ForeignKey(Valuation, on_delete=models.CASCADE)
    approved = models.IntegerField()
    customer = JSONField()
    charge = JSONField()
