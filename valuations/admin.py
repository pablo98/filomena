from django.contrib import admin

from . import models

admin.site.register(models.Valuation)
admin.site.register(models.Coupon)
admin.site.register(models.CouponTransaction)
admin.site.register(models.StripeTransaction)
