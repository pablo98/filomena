import os
import logging
import traceback

from django.conf import settings
from django.core.cache import cache
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views.decorators.http import require_safe, require_POST

from felicitas import predict
import valuations.email as valuation_email
import valuations.form as valuation_form
from valuations.models import Valuation
from valuations.stripe import charge_service
from valuations.coupon import redeem_coupon

# @todo maybe refactor logger usage
logger = logging.getLogger('filomena')


@require_safe
def index(request):
    tmpl_vars = {
        'include_tracking': 1 if os.environ.get('FILOMENA_ENV') == 'prod' else 0
    }
    return render(request, 'index.html', tmpl_vars)


@require_POST
def confirm(request):
    # handle form
    form = valuation_form.ValuationForm(request.POST)
    if not form.is_valid():
        error_dict = {'error_html_list': form.errors}
        return render(request, 'index.html', error_dict)

    # probe valuation
    try:
        predicted_value = get_predicted_value(form.cleaned_data)
    except Exception as e:
        logger.error(str(e))
        error_dict = {'error_predictor': 'I was unable to get a prediction from these values. Sorry.'}
        return render(request, 'index.html', error_dict)

    # store it
    valuation = valuation_form.get_valuation_object(form, request.META, predicted_value)
    # @todo save to redis instead, only persist once paid
    valuation.save()

    # render page
    data_dict = {
        'stripe_key': settings.STRIPE_KEYS['publishable_key'],
        'submitted': valuation_form.get_confirmation_data(valuation),
        # @todo get a token from redis instead
        'valuation_token': valuation.id
    }
    return render(request, 'confirm.html', data_dict)


@require_POST
def show_gratis(request):
    # handle form
    form = valuation_form.ValuationForm(request.POST)
    if not form.is_valid():
        error_dict = {'error_html_list': form.errors}
        return render(request, 'index.html', error_dict)

    # probe valuation
    try:
        predicted_value = get_predicted_value(form.cleaned_data)
    except Exception as e:
        logger.error(
            'Exception: %s\nInput: %s\nTraceback: %s',
            str(e),
            form.cleaned_data,
            traceback.format_exc())
        error_dict = {'error_predictor': 'I was unable to get a prediction from these values. Sorry.'}
        return render(request, 'index.html', error_dict)

    # store it
    valuation = valuation_form.get_valuation_object(form, request.META, predicted_value)
    valuation.save()

    return HttpResponseRedirect(reverse('show_valuation', kwargs={'token': valuation.token}))


@require_POST
def charge(request):
    # handle params
    email = request.POST.get('stripeEmail')
    stripe_token = request.POST.get('stripeToken')
    valuation_id = request.POST.get('valuation_token')
    if not (email and stripe_token and valuation_id):
        logger.error('we need email, stripe_token and valuation token. Not found in here ' + vars(request))
        return render(request, 'charge_error.html', {'error_charge': 'Missing required data.'})

    # retrieve valuation
    try:
        valuation = Valuation.objects.get(id=valuation_id)
    except Exception as e:
        logger.error(str(e))
        return render(request, 'charge_error.html', {'error_charge': 'Unable to retrieve valuation request.'})

    # charge
    try:
        charge_service(300, email, stripe_token, valuation)
    except Exception as e:
        return render(request, 'charge_error.html', {'error_charge': str(e)})

    # send valuation email
    try:
        valuation_email.send_text_valuation_email(email, valuation)
    except Exception as e:
        logger.error('Unable to send mail to {}. {}'.format(email, str(e)))
        msg = 'We were able to generate the report but the email sending failed.'
        return render(request, 'charge_error.html', {'error_charge': msg})

    tmpl_vars = {
        'email': email,
        'predicted_value': valuation.predicted_value
    }
    return render(request, 'charge.html', tmpl_vars)


@require_POST
def apply_coupon(request):
    try:
        # handle form
        form = valuation_form.CouponForm(request.POST)
        if not form.is_valid():
            raise Exception(form.errors)

        # retrieve valuation
        valuation = Valuation.objects.get(id=form.cleaned_data['valuation_token'])

        # redeem
        redeem_coupon(form.cleaned_data['coupon_code'], valuation)

    except Exception as e:
        logger.error(str(e))
        return render(
            request,
            'apply_coupon_error.html',
            {'error': 'Code might be expired, exhausted, non-existing or plainly refusing to be redeemed.'})

    tmpl_vars = {
        'predicted_value': valuation.predicted_value
    }
    return render(request, 'apply_coupon.html', tmpl_vars)


@require_safe
def show_valuation(request, token):
    if not token:
        logger.error('We need valuation token. Not found in here ' + vars(request))
        return render(request, 'show_valuation_error.html', {'error': 'Missing required data.'})

    # retrieve valuation
    try:
        valuation = Valuation.objects.get(token=token)
    except Exception as e:
        logger.error(str(e), vars(request.GET))
        return render(request, 'show_valuation_error.html', {'error': 'Unable to retrieve valuation request.'})

    # render page
    data_dict = {
        'submitted': valuation_form.get_confirmation_data(valuation),
        'predicted_value': valuation.predicted_value,
    }
    return render(request, 'show_valuation.html', data_dict)


def get_predicted_value(form_data):
    key = str(hash(frozenset(form_data.items())))
    cached_value = cache.get(key)
    if cached_value is not None:
        return cached_value

    value = _get_predicted_value(form_data)
    cache.set('predicted_value:' + key, value, 3600)

    return value


def _get_predicted_value(form_data):
    predicted_value = predict.predict_from_raw_features(form_data)
    predicted_value *= 1.15
    return int(predicted_value)
