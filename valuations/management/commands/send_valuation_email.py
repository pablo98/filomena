from django.core.management.base import BaseCommand, CommandError
from valuations.models import Valuation
import valuations.email as valuation_email


class Command(BaseCommand):
    help = 'Sends an existing valuation to an email recipient'

    def add_arguments(self, parser):
        parser.add_argument('valuation_id', nargs='+', type=int)
        parser.add_argument('to', nargs='+', type=str)

    def handle(self, *args, **options):
        to = options['to'][0]
        valuation_id = options['valuation_id'][0]
        try:
            valuation = Valuation.objects.get(pk=valuation_id)
            valuation_email.send_text_valuation_email(to, valuation)

        except Valuation.DoesNotExist:
            raise CommandError('Valuation "%s" does not exist' % valuation_id)

        self.stdout.write(self.style.SUCCESS('Successfully sent valuation "{}" to "{}"'.format(valuation_id, to)))
