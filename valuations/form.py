import hashlib
import random
import time

from django import forms
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.utils import timezone

from valuations.models import Valuation


class ValuationForm(forms.Form):
    postcode = forms.CharField(
        validators=[RegexValidator(regex='\s*\d{4}\s*[A-Za-z]{2}\s*')],
        label='Postcode',
        min_length=6,
        max_length=10)
    floor_number = forms.IntegerField(min_value=0, max_value=25)
    indoor_area = forms.FloatField(min_value=10, max_value=500)
    garden_area = forms.FloatField(min_value=0, max_value=5000, required=False)
    energielabel = forms.CharField(label='Energy Label')
    lift = forms.BooleanField(label='Has lift', required=False)
    annual_lease_amount = forms.FloatField(min_value=0, max_value=10000, required=False)

    def validate_energie_label(label):
        valid_labels = ['A', 'B', 'C', 'D', 'E', 'F', 'G']
        if label not in valid_labels:
            raise ValidationError('Energy label must be one of these ' + valid_labels)

    def clean_postcode(self):
        return self.cleaned_data['postcode'].replace(' ', '').upper()

    def clean_garden_area(self):
        if self.cleaned_data['garden_area']:
            return int(self.cleaned_data['garden_area'])
        else:
            return 0

    def clean_indoor_area(self):
        return int(self.cleaned_data['indoor_area'])

    def clean_annual_lease_amount(self):
        if self.cleaned_data['annual_lease_amount']:
            return int(self.cleaned_data['annual_lease_amount'])
        else:
            return 0

    def clean_lift(self):
        return 1 if self.cleaned_data['lift'] else 0


class CouponForm(forms.Form):
    coupon_code = forms.CharField(
        validators=[RegexValidator(regex='[a-zA-Z0-9]{5,10}')],
        label='Coupon Code')
    valuation_token = forms.CharField()


def get_confirmation_data(v):
    return {
        'postcode': v.postcode,
        'floor': get_floor_description(v.floor_number, v.lift),
        'indoor_area': '{} squared meters'.format(v.indoor_area),
        'garden_area': '{} squared meters'.format(v.garden_area) if v.garden_area else 'No garden',
        'annual_lease': '{} EUR'.format(v.annual_lease_amount) if v.annual_lease_amount else 'No annual leasehold',
        'energielabel': '"' + v.energielabel + '"'
    }


def get_floor_description(n, l):
    if n == 0:
        return 'Ground floor.'

    return '{}, by {}'.format(
        "%d%s" % (n, "tsnrhtdd"[(n / 10 % 10 != 1) * (n % 10 < 4) * n % 10::4]),
        'elevator' if l else 'the stairs')


def get_valuation_object(form, request_headers, predicted_value):
    input_fields = [
        'postcode',
        'floor_number',
        'indoor_area',
        'garden_area',
        'energielabel',
        'lift',
        'annual_lease_amount']

    valuation_dict = {k: form.cleaned_data[k] for k in input_fields}
    valuation_dict['created_at'] = timezone.now()
    valuation_dict['predicted_value'] = predicted_value
    salt = str(time.time() + predicted_value + random.randint(0, 100000))
    valuation_dict['token'] = hashlib.sha1(salt.encode('utf-8')).hexdigest()[:16]
    valuation_dict['user_fingerprint'] = get_user_fingerprint(request_headers)
    valuation = Valuation(**valuation_dict)

    return valuation


def get_user_fingerprint(h):
    try:
        salt = h['HTTP_ACCEPT_LANGUAGE'] + h['HTTP_USER_AGENT'] + h['HTTP_ACCEPT_ENCODING']
    except KeyError:
        return None

    return hashlib.sha1(salt.encode('utf-8')).hexdigest()[:20]
