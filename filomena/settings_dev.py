from filomena.settings_base import * # noqa

ALLOWED_HOSTS = [
    'filomena.localhost',
]

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_COMPANY = 'info@amsterprice.com'


# using redis to cache pages
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/0",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "IGNORE_EXCEPTIONS": True,
        },
        # in dev we want to make sure Redis works but don't really cache
        "TIMEOUT": 0
    },
}
