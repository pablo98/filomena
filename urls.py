from django.conf.urls import url
# from django.contrib import admin

import valuations.views as valuations_views

urlpatterns = [
    url(r'^$', valuations_views.index, name='index'),
    # url(r'^confirm/$', valuations_views.confirm, name='confirm'),
    # url(r'^charge/$', valuations_views.charge, name='charge'),
    # url(r'^apply_coupon/$', valuations_views.apply_coupon, name='apply_coupon'),
    url(r'^show_gratis/$', valuations_views.show_gratis, name='show_gratis'),
    url(r'^valuation/(?P<token>\w+)/$', valuations_views.show_valuation, name='show_valuation'),
    # url(r'^admin/', admin.site.urls),
]
